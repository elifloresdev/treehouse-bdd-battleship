"use strict"
const _ = require("underscore")

function checkForShip(player,coordinates) {
  const {ships} = player
  
  let shipPresent
    , ship

      for(let i = 0; i < ships.length; i++) {
          ship = ships[i]

          shipPresent = ship.locations.filter( function(actualCoordinate) {
            return (actualCoordinate[0] === coordinates[0] ) && (actualCoordinate[1] === coordinates[1])
          })[0]

          if(shipPresent) {
            return ship
          } 
      }
      return false
}

function damageShip(ship, coordinates) {
    ship.damage.push(coordinates)
}


function fire(player,coordinates) {

    const ship = checkForShip(player,coordinates)

    if(ship) {
      damageShip(ship,coordinates);
    }

}

module.exports.checkForShip = checkForShip
module.exports.damageShip = damageShip
module.exports.fire = fire