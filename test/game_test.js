"use strict"
const expect = require('chai').expect;

describe('GAME INSTANCE FUNCTIONS', function () {
  describe('checkGameStatus', function () {
    const checkGameStatus = require('../game_logic/game_instance.js').checkGameStatus;
    it('should tell me when the game is over', function () {
      const players = [
        {
					ships: [
						{
							locations: [[0, 0]],
							damage: [[0, 0]]
						}
					]
        }
      ];
      var actual = checkGameStatus(players);
      expect(actual).to.be.false;
    });
  });

  describe("takeTurn", () => {
      const takeTurn = require('../game_logic/game_instance').takeTurn
      let guess, player
      
      beforeEach(() => {
          guess = function() {return [0 ,0]} // stub
          player = {
              ships: [
                  {
                      locations: [[0,0]],
                      damage: []
                  }
              ]
          }
      })
      it("it should return false if the game ends", () => {
        const actual = takeTurn(player, guess)
        expect(actual).to.be.false
      })
  })

  function saveGame(callback) {
      setTimeout(() => {
          callback()
      },1000)
  }
  describe("saveGame", function() {
      it('should update save status', (done) => {  // done < asynchronous
          let status = 'game not save...'

          saveGame(() => {
            status = 'game saved'
            expect(status).to.be.equal('game saved')
            done() // done < asynchronous
          })

          

      })
  })
});